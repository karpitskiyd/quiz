
// i changed))
// i changed again))
// i changed again3))
// i changed again4))
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var answer1Button: UIButton!
    
    @IBOutlet weak var answer2Button: UIButton!
    
    @IBOutlet weak var answer3Button: UIButton!
    
    @IBOutlet weak var progressBar: UIImageView!
    @IBOutlet weak var progressBarLine: UIProgressView!
    @IBOutlet weak var scoreLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUi()
    }
    
    
    
    var quizBrain = QuizBrain()
    @IBAction func answerButtonPrassed(_ sender: UIButton) {
        
        let userAnswer = sender.currentTitle!  // 1 2 3 button
        let userGotItRight = quizBrain.checkAnswer(userAnswer)

        if userGotItRight {


            sender.backgroundColor = UIColor.green
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                sender.backgroundColor = UIColor.clear

        }
        }
        else
        {

            sender.backgroundColor = UIColor.red
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                sender.backgroundColor = UIColor.clear

        }

        }

        scoreLabel.text = "Score:\(quizBrain.getScore(userAnswer))"
        quizBrain.nextQuestion()
        updateUi()
        
        
        
    }
    
    
    func updateUi()
    
    {
        answer1Button.setTitle(quizBrain.getButton1Text(), for: .normal)
        answer2Button.setTitle(quizBrain.getButton2Text(), for: .normal)
        answer3Button.setTitle(quizBrain.getButton3Text(), for: .normal)
        questionLabel.text = quizBrain.getQuestionText()
        progressBarLine.progress = quizBrain.getProgress()
       
//        trueButton.backgroundColor = UIColor.clear
//        falseButton.backgroundColor = UIColor.clear
        
    }
    
}

